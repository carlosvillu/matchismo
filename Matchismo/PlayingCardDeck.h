//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Carlos Villuendas Zambrana on 23/11/13.
//  Copyright (c) 2013 Carlos Villuendas Zambrana. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
