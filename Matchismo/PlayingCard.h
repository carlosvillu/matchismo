//
//  PlayingCard.h
//  Matchismo
//
//  Created by Carlos Villuendas Zambrana on 23/11/13.
//  Copyright (c) 2013 Carlos Villuendas Zambrana. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;

@end
