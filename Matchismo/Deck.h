//
//  Deck.h
//  Matchismo
//
//  Created by Carlos Villuendas Zambrana on 23/11/13.
//  Copyright (c) 2013 Carlos Villuendas Zambrana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject


- (void)addCard:(Card *)card atTop:(BOOL)atTop;
- (void)addCard:(Card *)card;

- (Card *)drawRandomCard;

@end
