//
//  ViewController.m
//  Matchismo
//
//  Created by Carlos Villuendas Zambrana on 22/11/13.
//  Copyright (c) 2013 Carlos Villuendas Zambrana. All rights reserved.
//

#import "ViewController.h"
#import "PlayingCardDeck.h"
#import "Card.h"

@interface ViewController ()
@property (strong, nonatomic) Deck *deck;
@property (weak, nonatomic) IBOutlet UILabel *flips;
@property (nonatomic) int flipCount;
@end

@implementation ViewController

- (Deck *)deck {
    if (!_deck) _deck = [self createDeck];
    return _deck;
}

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc]init];
};

- (void)setFlipCount:(int)flipCount
{
    _flipCount = flipCount;
    self.flips.text = [NSString stringWithFormat:@"Taps: %d", self.flipCount];
};

- (IBAction)touchCardButton:(UIButton *)sender {
    
    if ([sender.currentTitle length]) {
        [sender setBackgroundImage:[UIImage imageNamed:@"cardback"]
                         forState:UIControlStateNormal];
        
        [sender setTitle:@"" forState:UIControlStateNormal];
        self.flipCount++;
    } else {
        Card *card = [self.deck drawRandomCard];
        if(card)
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"cardfront"]
                              forState: UIControlStateNormal];
            [sender setTitle:[card contents]
                    forState:UIControlStateNormal];
            self.flipCount++;
        }
    }
}

@end
