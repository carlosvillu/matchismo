//
//  main.m
//  Matchismo
//
//  Created by Carlos Villuendas Zambrana on 22/11/13.
//  Copyright (c) 2013 Carlos Villuendas Zambrana. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
